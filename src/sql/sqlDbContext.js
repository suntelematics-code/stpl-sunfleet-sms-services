const { sqlConnectionPool } = require("./connection");
const sql = require('mssql');

// Get the List of SMS Alerts whose status is 0 to send SMS. 
async function getSmsAlertList() {
    try {
        const pool = await sqlConnectionPool
        const result = await pool.request().execute("getSmsAlerts");
        return result.recordset;
    } catch (error) {
        throw new Error(error);
    }
}

async function updateRecievedSmsList(ids) {
    try {
        console.log('ids', ids);
        const pool = await sqlConnectionPool
        const query = `UPDATE smsAlerts SET status=2 WHERE CONVERT(VARCHAR, id) in (${ids})`
        console.log(query);
        const result = await pool.request()
            .query(query);
        return result.recordset;
    } catch (error) {
        throw new Error(error);
    }
}

async function updateSentOrFaildSmsStatus(smsResponse) {
    try {
       
        const pool = await sqlConnectionPool
        const query = `UPDATE smsAlerts SET status=${smsResponse.status}, responseCode='${smsResponse.responseCode}' WHERE id = ${smsResponse.id}`
        console.log(query);
        const result = await pool.request()
            .query(query);
        return result.recordset;
    } catch (error) {
        throw new Error(error);
    }
}

module.exports = {
    getSmsAlertList,
    updateRecievedSmsList,
    updateSentOrFaildSmsStatus
}
