const sql = require('mssql');
const { settings } = require('../config/config')

// Global SQL connection
const sqlConnectionPool = new sql.ConnectionPool(settings.sql)
    .connect()
    .then(pool => {
        console.log('Connected to MSSQL');
        return pool;
    })
    .catch(err => console.log('Database Connection Failed!', err));

sql.on('error', err => {
    console.log(err.message);
});

module.exports = {
    sqlConnectionPool
}
