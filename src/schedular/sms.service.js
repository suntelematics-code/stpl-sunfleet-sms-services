const schedule = require('node-schedule');
const request = require('request');
const dbUtility = require("../sql/sqlDbContext");
const { settings } = require('../config/config')


// Update the Sms status and responsecode which is from Knowlarity request 
async function updateSentOrFailedSmsStatus(sentSmsList) {
    for (let i = 0; i < sentSmsList.length; i++) {
        await dbUtility.updateSentOrFaildSmsStatus(sentSmsList[i]);
    }
}

/* Function responsible for creating full request URL which has to be sent.
sample URL: http://sms1x.knowlarity.com/api/pushsms?user=TXICABTrans&authkey=923xzuwFvs0&sender=SUNTLE&mobile=9036676467&text=Your one time password for registration is 3562 - SUNTEL&rpt=1&entityid=1001043296023960885&templateid=1007263976360962113
each parameter is declared separately and joined by '&' 
so that it will be more readable and in future it will be easy to add or remove parameter from URL
*/
function createSmsRequestUrl(smsDetails) {
    const user = `user=${settings.sms.user}`;
    const url = `${settings.sms.url}?${user}`;

    const authKey = `authkey=${settings.sms.authKey}`;
    const sender = `sender=${settings.sms.sender}`;
    const mobile = `mobile=${smsDetails.mobileNo}`;
    const text = `text=${smsDetails.bodyContent}`;
    const rpt = 'rpt=1';
    const entityId = `entityid=${smsDetails.entityId}`;
    const templateId = `templateid=${smsDetails.templateId}`;

    const fullUrl = [url, authKey, sender, mobile, text, rpt, entityId, templateId].join('&')
    console.log('Knowlarity SMS Url: ' + fullUrl);
    return fullUrl;
}

// promise function to send request to knowlarity
function sendSmsRequest(sms, requestUrl) {
    return new Promise((resolve, reject) => {
        request.get(requestUrl, async (err, _resp, body) => {
            sms.responseCode = body;
            if (err) {
                sms.responseCode = err;
                sms.status = 3; // failed
                console.log(sms.mobileNo, 'SMSError: ' + err);
            } else if (body.includes('OK')) {
                sms.status = 1; //success
                console.log('Sent to ' +sms.mobileNo, 'Response Body: ' + body);
            } else {
                sms.status = 3; // failed
                console.log(sms.mobileNo, body);
            }
            resolve(sms);
        });
    })
}


/* Function responsible for creating SMS request Url and sending SMS to mobileNo,
once SMS request Url returns the response body status as 'OK' then update the sms status to 1
*/
async function sendSms(smsAlerts) {
    for (let i = 0; i < smsAlerts.length; i++) {
        const requestUrl = createSmsRequestUrl(smsAlerts[i]);
        smsAlerts[i] = await sendSmsRequest(smsAlerts[i],requestUrl);
    }
    return smsAlerts;
}

// Schedules a job every one minute to send SMS.
schedule.scheduleJob('*/1 * * * *', async () => {
    const smsAlerts = await dbUtility.getSmsAlertList();
    console.log('smsAlerts data length: '+ smsAlerts.length);
    if (smsAlerts.length > 0) {
        const ids = smsAlerts.map(e => Number(e.id)).join(',');
        await dbUtility.updateRecievedSmsList(ids);
        const sentSmsList = await sendSms(smsAlerts);
        await updateSentOrFailedSmsStatus(sentSmsList);
    } else {
        console.log('No SMSAlerts available');
    }
});