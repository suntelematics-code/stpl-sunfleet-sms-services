
require('dotenv').config()
exports.settings = {
    sms: {
        url: "http://sms1x.knowlarity.com/api/pushsms",
        user: "TXICABTrans",
        authKey: "923xzuwFvs0",
        sender: "SUNTLE"
    },
    sql: {
        user: process.env.SQL_USERNAME,
        password: process.env.SQL_SUN_PASSWORD,
        database: process.env.SQL_SUN_DB,
        server: "10.0.1.223",
        pool: {
            max: 10,
            min: 0,
            idleTimeoutMillis: 30000
        },
        options: {
            encrypt: true, // for azure
            trustServerCertificate: true // change to true for local dev / self-signed certs
        }
    }
}